﻿// Andros, Justin
// jandros

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace CSHP210
{
    public enum Flavor
    {
        REGULAR,
        ORANGE,
        LEMON
    }

    // Extension used to have more descriptive flavor names
    public static class FlavorOps
    {
        private static List<Flavor> flavors = new List<Flavor>();

        static FlavorOps()
        {
            Debug.WriteLine("FlavorOps > FlavorOps()");
            foreach (string flavorName in Enum.GetNames(typeof(Flavor)))
            {
                Flavor flavorEnum = ToFlavor(flavorName);
                flavors.Add(flavorEnum);
            }
        }

        public static Flavor ToFlavor(string flavorName)
        {
            Debug.WriteLine("FlavorOps > Flavor ToFlavor(string)");
            flavorName = flavorName.ToUpper();
            if (Enum.IsDefined(typeof(Flavor), flavorName))
            {
                return (Flavor)Enum.Parse(typeof(Flavor), flavorName);
            }
            else
            {
                throw new VENDBADFLAVORException("Unknown flavor", flavorName);
            }
        }

        public static List<Flavor> AllFlavors
        {
            get
            {
                return flavors;
            }
        }
    }
}
