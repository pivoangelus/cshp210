﻿// Andros, Justin
// jandros

using System;

namespace CSHP210
{
    class VENDBADFLAVORException : Exception
    {
        public VENDBADFLAVORException() : base("Flavor cannot be found in list.")
        {           
        }

        public VENDBADFLAVORException(string msg, string flavorName) : base(string.Format("{0}: {1}", msg, flavorName))
        {
        }

        public VENDBADFLAVORException(string msg, string flavorName, Exception innerException) : base(string.Format("{0}: {1}\n{2}", msg, flavorName), innerException)
        {
        }
    }
}
