﻿// Andros, Justin
// jandros

using System.Diagnostics;

namespace CSHP210
{
    class Can
    {
        public readonly Flavor TheFlavor = Flavor.REGULAR; 

        public Can()
        {
            Debug.WriteLine("Can > Can()");
        }

        public Can(Flavor AFlavor)
        {
            Debug.WriteLine("Can > Can(Flavor)");
            TheFlavor = AFlavor;
        }
    }
}
