﻿// Andros, Justin
// jandros

using System.Diagnostics;

namespace CSHP210
{
    class PurchasePrice
    {
        private decimal price;

        public PurchasePrice()
        {
            Debug.WriteLine("PurchasePrice > PurchasePrice()");

            price = 0m;
        }

        // Deprecated use of PurchasePrice, taken from example code
        [System.Obsolete("Use PurchasePrice(decimal)", false)]  
        public PurchasePrice(int initialPrice)
        {
            Debug.WriteLine("PurchasePrice > PurchasePrice(int)");

            Price = initialPrice;
        }

        public PurchasePrice(decimal initialPrice)
        {
            Debug.WriteLine("PurchasePrice > PurchasePrice(demical)");

            PriceDecimal = initialPrice;
        }

        // Deprecated use of PurchasePrice, taken from example code
        [System.Obsolete("Use PriceDecimal", false)]
        public int Price
        {
            get
            {
                return (int)(price * 100);
            }

            set
            {
                price = value / 100m;
            }
        }

        public decimal PriceDecimal
        {
            get
            {
                return price;
            }

            set
            {
                price = value;
            }
        }
    }
}
