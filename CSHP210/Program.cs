﻿// Andros, Justin
// jandros

using System;
using System.Diagnostics;

namespace CSHP210
{
    class Program
    {
        static void Main(string[] args)
        {
            Debug.WriteLine("Main(string[])");

            PurchasePrice purchasePrice = new PurchasePrice(0.35m);
            CanRack canRack = new CanRack();
            CoinBox coinBox = new CoinBox();

            Console.WriteLine("Welcome to the .NET C# Soda Vending Machine.");

            bool continueVending = false;
            do
            {
                canRack.DisplayCanRack();
                decimal runningCost = 0m;
                do
                {
                    Console.Write("Please insert {0:c} cents (Currently inserted: {1:c}): ", purchasePrice.PriceDecimal, runningCost);
                    string inputCost = Console.ReadLine();

                    if (Enum.TryParse<Coin.Denomination>(inputCost, true, out Coin.Denomination coin))
                    {
                        Coin newCoin = new Coin(coin);
                        coinBox.Deposit(newCoin);
                        runningCost += newCoin.ValueOf;
                    }
                    else
                    {
                        Console.WriteLine("Coin doesn't exist");
                    }
                } while (runningCost < purchasePrice.PriceDecimal);

                bool flavorChosen = false;
                while (!flavorChosen)
                {
                    Console.Write("Please select a flavor: ");
                    string inputFlavor = Console.ReadLine().ToUpper();
                    try
                    {
                        Flavor flavor = FlavorOps.ToFlavor(inputFlavor);
                        if (!canRack.IsEmpty(flavor))
                        {
                            canRack.RemoveACanOf(flavor);
                            Console.WriteLine("Enjoy your {0} soda!", flavor);
                            flavorChosen = true;
                        }
                        else
                        {
                            Console.WriteLine("{0} is no longer in stock.", flavor);
                        }
                    }
                    catch (VENDBADFLAVORException e)
                    {
                        Console.WriteLine(e.Message);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("You have done something very wrong. Bas user, bad.");
                        Debug.WriteLine("Unkown Exception: " + e.Message);
                    }
                }

                Console.Write("Do you wish to continue? (y/n): ");
                string inputContinue = Console.ReadLine();
                continueVending = inputContinue.Trim().ToUpper().StartsWith("Y");
            } while (continueVending);

            coinBox.DisplayCoinBox();
            Console.ReadLine();
        }
    }
}
