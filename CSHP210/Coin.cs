﻿// Andros, Justin
// jandros

using System.Diagnostics;

namespace CSHP210
{
    class Coin
    {
        public enum Denomination
        {
            SLUG = 0,
            NICKEL = 5,
            DIME = 10,
            QUARTER = 25,
            HALFDOLLAR = 50
        }

        private Denomination coinEnumeral;

        public Coin()
        {
            Debug.WriteLine("Coin > Coin()");
            coinEnumeral = Denomination.SLUG;
        }

        public Coin(Denomination CoinEnumeral)
        {
            Debug.WriteLine("Coin > Coin(Denomination)");
            coinEnumeral = CoinEnumeral;
        }

        public Coin(string CoinName)
        {
            Debug.WriteLine("Coin > Coin(string)");
            if (System.Enum.IsDefined(typeof(Denomination), CoinName.ToUpper()) && System.Enum.TryParse<Denomination>(CoinName.ToUpper(), out Denomination enumeral))
            {
                coinEnumeral = enumeral;
            }
            else
            {
                coinEnumeral = Denomination.SLUG;
            }
        }

        public Coin(decimal CoinValue)
        {
            Debug.WriteLine("Coin > Coin(decimal)");
            Denomination d = (Denomination)(CoinValue * 100);
            switch (d)
            {
                case Denomination.NICKEL:
                case Denomination.DIME:
                case Denomination.QUARTER:
                case Denomination.HALFDOLLAR:
                    coinEnumeral = d;
                    break;
                default:
                    coinEnumeral = Denomination.SLUG;
                    break;
            }
        }

        public decimal ValueOf
        {
            get
            {
                return (decimal)coinEnumeral / 100;
            }
        }

        public Denomination CoinEnumeral
        {
            get
            {
                return coinEnumeral;
            }
        }

        public override string ToString()
        {
            Debug.WriteLine("Coin > string ToString()");
            return System.Enum.GetName(typeof(Denomination), coinEnumeral);
        }
    }
}
