﻿// Andros, Justin
// jandros

using System;
using System.Diagnostics;
using System.Collections.Generic;

namespace CSHP210
{
    class CanRack
    {
        private const int MINRACK = 0;
        private const int MAXRACK = 3;

        private Dictionary<Flavor, int> canRack = null;

        // Constructorfor a can rack. The rack starts out full
        public CanRack()
        {
            Debug.WriteLine("CanRack > CanRack()");

            // Create dictionary that matches the key ignoring case
            canRack = new Dictionary<Flavor, int>();   
            // Max out the rack
            FillTheCanRack();
        }

        //  This method adds a can of the specified flavor to the rack.  
        public void AddACanOf(Flavor FlavorOfCanToBeAdded)
        {
            Debug.WriteLine("CanRack > AddACanOf(Flavor)");

            // See if the flavor exists
            if (canRack.TryGetValue(FlavorOfCanToBeAdded, out int value))
            {
                // See if the flavor is full
                if (!IsFull(FlavorOfCanToBeAdded))
                {
                    // Add a can
                    canRack[FlavorOfCanToBeAdded] += 1;
                    Debug.WriteLine(FlavorOfCanToBeAdded + " = " + canRack[FlavorOfCanToBeAdded]);
                }
                else
                {
                    // Can't add anymore cans
                    Debug.WriteLine("Cannot add any more {0} cans. This flavor is already maxed ({1})", FlavorOfCanToBeAdded, MAXRACK);
                }
            }
            else
            {
                // Can't find a matching flavor
                Debug.WriteLine("Cannot find {0} flavor.", FlavorOfCanToBeAdded);
            }
        }

        //  This method will remove a can of the specified flavor from the rack.
        public void RemoveACanOf(Flavor FlavorOfCanToBeRemoved)
        {
            Debug.WriteLine("CanRack > RemoveACanOf(Flavor)");

            // See if the flavor exists
            if (canRack.TryGetValue(FlavorOfCanToBeRemoved, out int value))
            {
                // See if the flavor is empty
                if (!IsEmpty(FlavorOfCanToBeRemoved))
                {
                    // Remove a can
                    canRack[FlavorOfCanToBeRemoved] -= 1;
                    Debug.WriteLine(FlavorOfCanToBeRemoved + " = " + canRack[FlavorOfCanToBeRemoved]);
                }
                else
                {
                    // No cans to remove
                    Debug.WriteLine("Cannot remove any more {0} cans. This flavor is out of stock.", FlavorOfCanToBeRemoved);
                }
            }
            else
            {
                // Can't find a matching flavor
                Debug.WriteLine("Cannot find {0} flavor.", FlavorOfCanToBeRemoved);
            }
        }

        //  This method will fill the can rack.
        public void FillTheCanRack()
        {
            Debug.WriteLine("CanRack > FillTheCanRack()");

            foreach (int i in Enum.GetValues(typeof(Flavor)))
            {
                canRack[(Flavor)i] = MAXRACK;
                Debug.WriteLine((Flavor)i + " = " + canRack[(Flavor)i]);
            }
        }

        //  This public void will empty the rack of a given flavor.
        public void EmptyCanRackOf(Flavor FlavorOfBinToBeEmptied)
        {
            Debug.WriteLine("CanRack > EmptyCanRackOf(string)");

            // See if the flavor exists
            if (canRack.ContainsKey(FlavorOfBinToBeEmptied))
            {
                // Empty the rack of requested flavor
                canRack[FlavorOfBinToBeEmptied] = MINRACK;
                Debug.WriteLine(FlavorOfBinToBeEmptied + " = " + canRack[FlavorOfBinToBeEmptied]);
            }
            else
            {
                // Can't find a matching flavor
                Debug.WriteLine("{0} is not a flavor to empty.", FlavorOfBinToBeEmptied);
            }
        }

        public Boolean IsFull(Flavor FlavorOfBinToCheck)
        {
            Debug.WriteLine("CanRack > Boolean IsFull(Flavor)");

            // See if the flavor exists and if it's maxed
            if (canRack.TryGetValue(FlavorOfBinToCheck, out int value) && value == MAXRACK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
            
        public Boolean IsEmpty(Flavor FlavorOfBinToCheck)
        {
            Debug.WriteLine("CanRack > Boolean IsEmpty(Flavor)");

            // See if the flavor exists and if it's empty
            if (canRack.TryGetValue(FlavorOfBinToCheck, out int value) && value == MINRACK)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void DisplayCanRack()
        {
            Debug.WriteLine("CanRack > DisplayCanRack()");
            Console.WriteLine("______________________________");
            Console.WriteLine("{0,-20} {1,8}","Soda Name","Quantity\n");
            foreach (int i in Enum.GetValues(typeof(Flavor)))
            {
                Console.WriteLine("{0,-20} {1,8}", (Flavor)i, canRack[(Flavor)i]);
            }
            Console.WriteLine("______________________________");
        }
    }
}
