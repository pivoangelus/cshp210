﻿// Andros, Justin
// jandros

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CSHP210
{
    class CoinBox
    {
        private List<Coin> coinBox;

        public CoinBox()
        {
            Debug.WriteLine("CoinBox > CoinBox()");
            coinBox = new List<Coin>();
        }

        public CoinBox(List<Coin> SeedMoney)
        {
            Debug.WriteLine("CoinBox > CoinBox(List<coin>)");
            coinBox = SeedMoney;
        }

        public void Deposit(Coin ACoin)
        {
            Debug.WriteLine("CoinBox > Deposit(Coin)");
            coinBox.Add(ACoin);
        }

        public Boolean Withdraw(Coin.Denomination ACoinDenomination)
        {
            Debug.WriteLine("CoinBox > Withdraw(Coin.Denomination)");
            var coins =
                from coin in coinBox
                where coin.CoinEnumeral == ACoinDenomination
                select coin;

            if (coins.Count() > 0)
            {
                return coinBox.Remove(coins.First());
            }
            return false;
        }

        private int CoinCount(Coin.Denomination denom)
        {
            Debug.WriteLine("CoinBox > int CoinCount(Coin.Denomination)");
            var coins =
                from coin in coinBox
                where coin.CoinEnumeral == denom
                select coin;

            return coins.Count();
        }

        public int HalfDollarCount
        {
            get
            {
                return CoinCount(Coin.Denomination.HALFDOLLAR);
            }
        }

        public int QuarterCount
        {
            get
            {
                return CoinCount(Coin.Denomination.QUARTER);
            }
        }

        public int DimeCount
        {
            get
            {
                return CoinCount(Coin.Denomination.DIME);
            }
        }

        public int NickelCount
        {
            get
            {
                return CoinCount(Coin.Denomination.NICKEL);
            }
        }

        public int SlugCount
        {
            get
            {
                return CoinCount(Coin.Denomination.SLUG);
            }
        }

        public decimal ValueOf
        {
            get
            {
                return
                    HalfDollarCount * (decimal)Coin.Denomination.HALFDOLLAR / 100m +
                    QuarterCount * (decimal)Coin.Denomination.QUARTER / 100m +
                    DimeCount * (decimal)Coin.Denomination.DIME / 100m +
                    NickelCount * (decimal)Coin.Denomination.NICKEL / 100m;
            }
        }

        public void DisplayCoinBox()
        {
            Debug.WriteLine("CoinBox > DisplayCoinBox()");
            Console.WriteLine("______________________________");
            Console.WriteLine("CoinBox");
            Console.WriteLine(" {0,-20} {1,8}", "Coin", "Quantity\n");
            foreach(Coin.Denomination c in Enum.GetValues(typeof(Coin.Denomination)))
            {
                Console.WriteLine(" {0,-20} {1,8}", c, CoinCount(c));
            }
            Console.WriteLine("______________________________");
        }
    }
}
