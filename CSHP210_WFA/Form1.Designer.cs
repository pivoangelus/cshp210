﻿namespace CSHP210_WFA
{
    partial class Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageVend = new System.Windows.Forms.TabPage();
            this.button_coinReturn = new System.Windows.Forms.Button();
            this.label_exactChangeRequiredStatic = new System.Windows.Forms.Label();
            this.label_insertForSoda = new System.Windows.Forms.Label();
            this.button_halfDollar = new System.Windows.Forms.Button();
            this.button_quarter = new System.Windows.Forms.Button();
            this.button_dime = new System.Windows.Forms.Button();
            this.button_nickel = new System.Windows.Forms.Button();
            this.label_totalInsertedStatic = new System.Windows.Forms.Label();
            this.label_totalInserted = new System.Windows.Forms.Label();
            this.button_select_lemon = new System.Windows.Forms.Button();
            this.button_select_orange = new System.Windows.Forms.Button();
            this.button_select_regular = new System.Windows.Forms.Button();
            this.pictureBox_regular = new System.Windows.Forms.PictureBox();
            this.pictureBox_orange = new System.Windows.Forms.PictureBox();
            this.pictureBox_lemon = new System.Windows.Forms.PictureBox();
            this.label_sodaTitleStatic = new System.Windows.Forms.Label();
            this.tabPageService = new System.Windows.Forms.TabPage();
            this.groupBoxCoinBox = new System.Windows.Forms.GroupBox();
            this.buttonEmptyCoinBox = new System.Windows.Forms.Button();
            this.listViewCoinBox = new System.Windows.Forms.ListView();
            this.columnHeaderCoinType = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCoinQuantity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.groupBoxCanRack = new System.Windows.Forms.GroupBox();
            this.buttonRefillCanRack = new System.Windows.Forms.Button();
            this.listViewCanRack = new System.Windows.Forms.ListView();
            this.columnHeaderFlavor = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderFlavorQuantity = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderCoinValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeaderTotalValue = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label_totalInsertedServiceStatic = new System.Windows.Forms.Label();
            this.label_totalInsertedService = new System.Windows.Forms.Label();
            this.tabControl.SuspendLayout();
            this.tabPageVend.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_regular)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_orange)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_lemon)).BeginInit();
            this.tabPageService.SuspendLayout();
            this.groupBoxCoinBox.SuspendLayout();
            this.groupBoxCanRack.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageVend);
            this.tabControl.Controls.Add(this.tabPageService);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(790, 718);
            this.tabControl.TabIndex = 0;
            this.tabControl.Click += new System.EventHandler(this.tabControl_click);
            // 
            // tabPageVend
            // 
            this.tabPageVend.Controls.Add(this.button_coinReturn);
            this.tabPageVend.Controls.Add(this.label_exactChangeRequiredStatic);
            this.tabPageVend.Controls.Add(this.label_insertForSoda);
            this.tabPageVend.Controls.Add(this.button_halfDollar);
            this.tabPageVend.Controls.Add(this.button_quarter);
            this.tabPageVend.Controls.Add(this.button_dime);
            this.tabPageVend.Controls.Add(this.button_nickel);
            this.tabPageVend.Controls.Add(this.label_totalInsertedStatic);
            this.tabPageVend.Controls.Add(this.label_totalInserted);
            this.tabPageVend.Controls.Add(this.button_select_lemon);
            this.tabPageVend.Controls.Add(this.button_select_orange);
            this.tabPageVend.Controls.Add(this.button_select_regular);
            this.tabPageVend.Controls.Add(this.pictureBox_regular);
            this.tabPageVend.Controls.Add(this.pictureBox_orange);
            this.tabPageVend.Controls.Add(this.pictureBox_lemon);
            this.tabPageVend.Controls.Add(this.label_sodaTitleStatic);
            this.tabPageVend.Location = new System.Drawing.Point(4, 22);
            this.tabPageVend.Name = "tabPageVend";
            this.tabPageVend.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageVend.Size = new System.Drawing.Size(782, 692);
            this.tabPageVend.TabIndex = 0;
            this.tabPageVend.Text = "Vend";
            this.tabPageVend.UseVisualStyleBackColor = true;
            // 
            // button_coinReturn
            // 
            this.button_coinReturn.Enabled = false;
            this.button_coinReturn.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_coinReturn.Location = new System.Drawing.Point(606, 644);
            this.button_coinReturn.Name = "button_coinReturn";
            this.button_coinReturn.Size = new System.Drawing.Size(90, 36);
            this.button_coinReturn.TabIndex = 38;
            this.button_coinReturn.Text = "Coin Return";
            this.button_coinReturn.UseVisualStyleBackColor = true;
            this.button_coinReturn.Click += new System.EventHandler(this.button_coinReturn_click);
            // 
            // label_exactChangeRequiredStatic
            // 
            this.label_exactChangeRequiredStatic.AutoSize = true;
            this.label_exactChangeRequiredStatic.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_exactChangeRequiredStatic.ForeColor = System.Drawing.Color.DarkRed;
            this.label_exactChangeRequiredStatic.Location = new System.Drawing.Point(57, 644);
            this.label_exactChangeRequiredStatic.Name = "label_exactChangeRequiredStatic";
            this.label_exactChangeRequiredStatic.Size = new System.Drawing.Size(179, 18);
            this.label_exactChangeRequiredStatic.TabIndex = 37;
            this.label_exactChangeRequiredStatic.Text = "Exact change required.";
            // 
            // label_insertForSoda
            // 
            this.label_insertForSoda.AutoSize = true;
            this.label_insertForSoda.Font = new System.Drawing.Font("Arial", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_insertForSoda.Location = new System.Drawing.Point(285, 73);
            this.label_insertForSoda.Name = "label_insertForSoda";
            this.label_insertForSoda.Size = new System.Drawing.Size(220, 18);
            this.label_insertForSoda.TabIndex = 36;
            this.label_insertForSoda.Text = "Please insert 35¢ for a soda.";
            // 
            // button_halfDollar
            // 
            this.button_halfDollar.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_halfDollar.Location = new System.Drawing.Point(60, 599);
            this.button_halfDollar.Name = "button_halfDollar";
            this.button_halfDollar.Size = new System.Drawing.Size(90, 36);
            this.button_halfDollar.TabIndex = 32;
            this.button_halfDollar.Tag = "50";
            this.button_halfDollar.Text = "50¢";
            this.button_halfDollar.UseVisualStyleBackColor = true;
            this.button_halfDollar.Click += new System.EventHandler(this.button_coinInserted_click);
            // 
            // button_quarter
            // 
            this.button_quarter.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_quarter.Location = new System.Drawing.Point(156, 599);
            this.button_quarter.Name = "button_quarter";
            this.button_quarter.Size = new System.Drawing.Size(90, 36);
            this.button_quarter.TabIndex = 33;
            this.button_quarter.Text = "25¢";
            this.button_quarter.UseVisualStyleBackColor = true;
            this.button_quarter.Click += new System.EventHandler(this.button_coinInserted_click);
            // 
            // button_dime
            // 
            this.button_dime.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_dime.Location = new System.Drawing.Point(252, 599);
            this.button_dime.Name = "button_dime";
            this.button_dime.Size = new System.Drawing.Size(90, 36);
            this.button_dime.TabIndex = 34;
            this.button_dime.Text = "10¢";
            this.button_dime.UseVisualStyleBackColor = true;
            this.button_dime.Click += new System.EventHandler(this.button_coinInserted_click);
            // 
            // button_nickel
            // 
            this.button_nickel.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_nickel.Location = new System.Drawing.Point(350, 599);
            this.button_nickel.Name = "button_nickel";
            this.button_nickel.Size = new System.Drawing.Size(90, 36);
            this.button_nickel.TabIndex = 35;
            this.button_nickel.Text = "5¢";
            this.button_nickel.UseVisualStyleBackColor = true;
            this.button_nickel.Click += new System.EventHandler(this.button_coinInserted_click);
            // 
            // label_totalInsertedStatic
            // 
            this.label_totalInsertedStatic.AutoSize = true;
            this.label_totalInsertedStatic.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_totalInsertedStatic.Location = new System.Drawing.Point(482, 608);
            this.label_totalInsertedStatic.Name = "label_totalInsertedStatic";
            this.label_totalInsertedStatic.Size = new System.Drawing.Size(118, 19);
            this.label_totalInsertedStatic.TabIndex = 31;
            this.label_totalInsertedStatic.Text = "Total Inserted:";
            // 
            // label_totalInserted
            // 
            this.label_totalInserted.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label_totalInserted.Cursor = System.Windows.Forms.Cursors.Default;
            this.label_totalInserted.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_totalInserted.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label_totalInserted.Location = new System.Drawing.Point(606, 597);
            this.label_totalInserted.Name = "label_totalInserted";
            this.label_totalInserted.Size = new System.Drawing.Size(90, 36);
            this.label_totalInserted.TabIndex = 30;
            this.label_totalInserted.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_select_lemon
            // 
            this.button_select_lemon.Enabled = false;
            this.button_select_lemon.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_select_lemon.Location = new System.Drawing.Point(606, 523);
            this.button_select_lemon.Name = "button_select_lemon";
            this.button_select_lemon.Size = new System.Drawing.Size(90, 36);
            this.button_select_lemon.TabIndex = 29;
            this.button_select_lemon.Text = "SELECT";
            this.button_select_lemon.UseVisualStyleBackColor = true;
            this.button_select_lemon.Click += new System.EventHandler(this.button_selectSoda_click);
            // 
            // button_select_orange
            // 
            this.button_select_orange.Enabled = false;
            this.button_select_orange.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_select_orange.Location = new System.Drawing.Point(345, 523);
            this.button_select_orange.Name = "button_select_orange";
            this.button_select_orange.Size = new System.Drawing.Size(90, 36);
            this.button_select_orange.TabIndex = 28;
            this.button_select_orange.Text = "SELECT";
            this.button_select_orange.UseVisualStyleBackColor = true;
            this.button_select_orange.Click += new System.EventHandler(this.button_selectSoda_click);
            // 
            // button_select_regular
            // 
            this.button_select_regular.Enabled = false;
            this.button_select_regular.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_select_regular.Location = new System.Drawing.Point(79, 523);
            this.button_select_regular.Name = "button_select_regular";
            this.button_select_regular.Size = new System.Drawing.Size(90, 36);
            this.button_select_regular.TabIndex = 27;
            this.button_select_regular.Text = "SELECT";
            this.button_select_regular.UseVisualStyleBackColor = true;
            this.button_select_regular.Click += new System.EventHandler(this.button_selectSoda_click);
            // 
            // pictureBox_regular
            // 
            this.pictureBox_regular.Image = global::CSHP210_WFA.Properties.Resources.royal_cat_regular;
            this.pictureBox_regular.Location = new System.Drawing.Point(14, 103);
            this.pictureBox_regular.Name = "pictureBox_regular";
            this.pictureBox_regular.Size = new System.Drawing.Size(232, 413);
            this.pictureBox_regular.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox_regular.TabIndex = 24;
            this.pictureBox_regular.TabStop = false;
            // 
            // pictureBox_orange
            // 
            this.pictureBox_orange.Image = global::CSHP210_WFA.Properties.Resources.royal_cat_orange;
            this.pictureBox_orange.Location = new System.Drawing.Point(278, 103);
            this.pictureBox_orange.Name = "pictureBox_orange";
            this.pictureBox_orange.Size = new System.Drawing.Size(232, 413);
            this.pictureBox_orange.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox_orange.TabIndex = 25;
            this.pictureBox_orange.TabStop = false;
            // 
            // pictureBox_lemon
            // 
            this.pictureBox_lemon.Image = global::CSHP210_WFA.Properties.Resources.royal_cat_lemon;
            this.pictureBox_lemon.Location = new System.Drawing.Point(537, 103);
            this.pictureBox_lemon.Name = "pictureBox_lemon";
            this.pictureBox_lemon.Size = new System.Drawing.Size(232, 413);
            this.pictureBox_lemon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox_lemon.TabIndex = 26;
            this.pictureBox_lemon.TabStop = false;
            // 
            // label_sodaTitleStatic
            // 
            this.label_sodaTitleStatic.AutoSize = true;
            this.label_sodaTitleStatic.Font = new System.Drawing.Font("Arial", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_sodaTitleStatic.Location = new System.Drawing.Point(174, 15);
            this.label_sodaTitleStatic.Name = "label_sodaTitleStatic";
            this.label_sodaTitleStatic.Size = new System.Drawing.Size(450, 56);
            this.label_sodaTitleStatic.TabIndex = 23;
            this.label_sodaTitleStatic.Text = "ROYAL CAT Sodas";
            // 
            // tabPageService
            // 
            this.tabPageService.Controls.Add(this.groupBoxCoinBox);
            this.tabPageService.Controls.Add(this.groupBoxCanRack);
            this.tabPageService.Location = new System.Drawing.Point(4, 22);
            this.tabPageService.Name = "tabPageService";
            this.tabPageService.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageService.Size = new System.Drawing.Size(782, 692);
            this.tabPageService.TabIndex = 1;
            this.tabPageService.Text = "Service";
            this.tabPageService.UseVisualStyleBackColor = true;
            // 
            // groupBoxCoinBox
            // 
            this.groupBoxCoinBox.Controls.Add(this.label_totalInsertedServiceStatic);
            this.groupBoxCoinBox.Controls.Add(this.label_totalInsertedService);
            this.groupBoxCoinBox.Controls.Add(this.buttonEmptyCoinBox);
            this.groupBoxCoinBox.Controls.Add(this.listViewCoinBox);
            this.groupBoxCoinBox.Location = new System.Drawing.Point(397, 6);
            this.groupBoxCoinBox.Name = "groupBoxCoinBox";
            this.groupBoxCoinBox.Size = new System.Drawing.Size(375, 680);
            this.groupBoxCoinBox.TabIndex = 2;
            this.groupBoxCoinBox.TabStop = false;
            this.groupBoxCoinBox.Text = "Coin Box";
            // 
            // buttonEmptyCoinBox
            // 
            this.buttonEmptyCoinBox.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEmptyCoinBox.Location = new System.Drawing.Point(256, 651);
            this.buttonEmptyCoinBox.Name = "buttonEmptyCoinBox";
            this.buttonEmptyCoinBox.Size = new System.Drawing.Size(112, 23);
            this.buttonEmptyCoinBox.TabIndex = 1;
            this.buttonEmptyCoinBox.Text = "Empty Coin Box";
            this.buttonEmptyCoinBox.UseVisualStyleBackColor = true;
            this.buttonEmptyCoinBox.Click += new System.EventHandler(this.button_emptyCoinBox_click);
            // 
            // listViewCoinBox
            // 
            this.listViewCoinBox.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderCoinType,
            this.columnHeaderCoinQuantity,
            this.columnHeaderCoinValue,
            this.columnHeaderTotalValue});
            this.listViewCoinBox.Location = new System.Drawing.Point(7, 20);
            this.listViewCoinBox.Name = "listViewCoinBox";
            this.listViewCoinBox.Size = new System.Drawing.Size(362, 625);
            this.listViewCoinBox.TabIndex = 0;
            this.listViewCoinBox.UseCompatibleStateImageBehavior = false;
            this.listViewCoinBox.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderCoinType
            // 
            this.columnHeaderCoinType.Text = "Coin Type";
            this.columnHeaderCoinType.Width = 100;
            // 
            // columnHeaderCoinQuantity
            // 
            this.columnHeaderCoinQuantity.Text = "Quantity";
            this.columnHeaderCoinQuantity.Width = 80;
            // 
            // groupBoxCanRack
            // 
            this.groupBoxCanRack.Controls.Add(this.buttonRefillCanRack);
            this.groupBoxCanRack.Controls.Add(this.listViewCanRack);
            this.groupBoxCanRack.Location = new System.Drawing.Point(10, 6);
            this.groupBoxCanRack.Name = "groupBoxCanRack";
            this.groupBoxCanRack.Size = new System.Drawing.Size(375, 680);
            this.groupBoxCanRack.TabIndex = 1;
            this.groupBoxCanRack.TabStop = false;
            this.groupBoxCanRack.Text = "Can Rack";
            // 
            // buttonRefillCanRack
            // 
            this.buttonRefillCanRack.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonRefillCanRack.Location = new System.Drawing.Point(264, 651);
            this.buttonRefillCanRack.Name = "buttonRefillCanRack";
            this.buttonRefillCanRack.Size = new System.Drawing.Size(103, 23);
            this.buttonRefillCanRack.TabIndex = 1;
            this.buttonRefillCanRack.Text = "Refill Can Rack";
            this.buttonRefillCanRack.UseVisualStyleBackColor = true;
            this.buttonRefillCanRack.Click += new System.EventHandler(this.button_refillCanRack_click);
            // 
            // listViewCanRack
            // 
            this.listViewCanRack.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeaderFlavor,
            this.columnHeaderFlavorQuantity});
            this.listViewCanRack.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewCanRack.Location = new System.Drawing.Point(6, 19);
            this.listViewCanRack.MultiSelect = false;
            this.listViewCanRack.Name = "listViewCanRack";
            this.listViewCanRack.Size = new System.Drawing.Size(361, 626);
            this.listViewCanRack.TabIndex = 0;
            this.listViewCanRack.UseCompatibleStateImageBehavior = false;
            this.listViewCanRack.View = System.Windows.Forms.View.Details;
            // 
            // columnHeaderFlavor
            // 
            this.columnHeaderFlavor.Text = "Flavor";
            this.columnHeaderFlavor.Width = 225;
            // 
            // columnHeaderFlavorQuantity
            // 
            this.columnHeaderFlavorQuantity.Text = "Quantity";
            this.columnHeaderFlavorQuantity.Width = 80;
            // 
            // columnHeaderCoinValue
            // 
            this.columnHeaderCoinValue.Text = "Value";
            // 
            // columnHeaderTotalValue
            // 
            this.columnHeaderTotalValue.Text = "Total Value";
            this.columnHeaderTotalValue.Width = 100;
            // 
            // label_totalInsertedServiceStatic
            // 
            this.label_totalInsertedServiceStatic.AutoSize = true;
            this.label_totalInsertedServiceStatic.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_totalInsertedServiceStatic.Location = new System.Drawing.Point(9, 654);
            this.label_totalInsertedServiceStatic.Name = "label_totalInsertedServiceStatic";
            this.label_totalInsertedServiceStatic.Size = new System.Drawing.Size(98, 16);
            this.label_totalInsertedServiceStatic.TabIndex = 33;
            this.label_totalInsertedServiceStatic.Text = "Total Inserted:";
            // 
            // label_totalInsertedService
            // 
            this.label_totalInsertedService.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.label_totalInsertedService.Cursor = System.Windows.Forms.Cursors.Default;
            this.label_totalInsertedService.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_totalInsertedService.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.label_totalInsertedService.Location = new System.Drawing.Point(113, 651);
            this.label_totalInsertedService.Name = "label_totalInsertedService";
            this.label_totalInsertedService.Size = new System.Drawing.Size(67, 23);
            this.label_totalInsertedService.TabIndex = 32;
            this.label_totalInsertedService.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 741);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(830, 780);
            this.MinimumSize = new System.Drawing.Size(830, 780);
            this.Name = "Form";
            this.Text = ".NET C# Vending Machine";
            this.tabControl.ResumeLayout(false);
            this.tabPageVend.ResumeLayout(false);
            this.tabPageVend.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_regular)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_orange)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_lemon)).EndInit();
            this.tabPageService.ResumeLayout(false);
            this.groupBoxCoinBox.ResumeLayout(false);
            this.groupBoxCoinBox.PerformLayout();
            this.groupBoxCanRack.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageVend;
        private System.Windows.Forms.Button button_coinReturn;
        private System.Windows.Forms.Label label_exactChangeRequiredStatic;
        private System.Windows.Forms.Label label_insertForSoda;
        private System.Windows.Forms.Button button_halfDollar;
        private System.Windows.Forms.Button button_quarter;
        private System.Windows.Forms.Button button_dime;
        private System.Windows.Forms.Button button_nickel;
        private System.Windows.Forms.Label label_totalInsertedStatic;
        private System.Windows.Forms.Label label_totalInserted;
        private System.Windows.Forms.Button button_select_lemon;
        private System.Windows.Forms.Button button_select_orange;
        private System.Windows.Forms.Button button_select_regular;
        private System.Windows.Forms.PictureBox pictureBox_regular;
        private System.Windows.Forms.PictureBox pictureBox_orange;
        private System.Windows.Forms.PictureBox pictureBox_lemon;
        private System.Windows.Forms.Label label_sodaTitleStatic;
        private System.Windows.Forms.TabPage tabPageService;
        private System.Windows.Forms.GroupBox groupBoxCoinBox;
        private System.Windows.Forms.Button buttonEmptyCoinBox;
        private System.Windows.Forms.ListView listViewCoinBox;
        private System.Windows.Forms.ColumnHeader columnHeaderCoinType;
        private System.Windows.Forms.ColumnHeader columnHeaderCoinQuantity;
        private System.Windows.Forms.GroupBox groupBoxCanRack;
        private System.Windows.Forms.Button buttonRefillCanRack;
        private System.Windows.Forms.ListView listViewCanRack;
        private System.Windows.Forms.ColumnHeader columnHeaderFlavor;
        private System.Windows.Forms.ColumnHeader columnHeaderFlavorQuantity;
        private System.Windows.Forms.ColumnHeader columnHeaderCoinValue;
        private System.Windows.Forms.ColumnHeader columnHeaderTotalValue;
        private System.Windows.Forms.Label label_totalInsertedServiceStatic;
        private System.Windows.Forms.Label label_totalInsertedService;
    }
}

