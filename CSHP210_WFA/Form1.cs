﻿using System;
using System.Windows.Forms;
using _06._0_Console_As_Library;

namespace CSHP210_WFA
{
    public partial class Form : System.Windows.Forms.Form
    {
        private CanRack canRack = new CanRack();
        private CoinBox coinBox = new CoinBox();
        private PurchasePrice purchasePrice = new PurchasePrice(0.35M);

        public Form()
        {
            InitializeComponent();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            label_insertForSoda.Text = string.Format("Please insert {0}¢ for a soda.", purchasePrice);
            updateCanRack();
        }

        private void placeIntoCoinBox(Coin c)
        {
            coinBox.Deposit(c);
            updateTotalInserted();
        }

        private void updateTotalInserted()
        {
            label_totalInserted.Text = string.Format("{0:c}", coinBox.ValueOf);
            button_coinReturn.Enabled = coinBox.ValueOf > 0M;
        }

        private void updateCanRack()
        {
            if (coinBox.ValueOf >= purchasePrice.PriceDecimal)
            {
                button_select_regular.Enabled = !canRack.IsEmpty(Flavor.REGULAR);
                button_select_orange.Enabled = !canRack.IsEmpty(Flavor.ORANGE);
                button_select_lemon.Enabled = !canRack.IsEmpty(Flavor.LEMON);
            }
            else
            {
                button_select_regular.Enabled = false;
                button_select_orange.Enabled = false;
                button_select_lemon.Enabled = false;
            }
        }

        private void button_coinReturn_click(object sender, EventArgs e)
        {
            MessageBox.Show(string.Format("{0:c} has been returned.", coinBox.ValueOf));
            emptyCoinBox(coinBox);
            updateTotalInserted();
            updateCanRack();
        }

        private void emptyCoinBox(CoinBox c)
        {
            CoinBox tempBox = new CoinBox();
            c.Transfer(tempBox);
            tempBox = null;
        }

        private void button_coinInserted_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b != null)
            {
                Coin c;
                switch (b.Name)
                {
                    case "button_halfDollar":
                        c = new Coin(Coin.Denomination.HALFDOLLAR);
                        break;
                    case "button_quarter":
                        c = new Coin(Coin.Denomination.QUARTER);
                        break;
                    case "button_dime":
                        c = new Coin(Coin.Denomination.DIME);
                        break;
                    case "button_nickel":
                        c = new Coin(Coin.Denomination.NICKEL);
                        break;
                    default:
                        c = new Coin(Coin.Denomination.SLUG);
                        break;
                }
                placeIntoCoinBox(c);
                updateTotalInserted();
            }

            if (coinBox.ValueOf >= purchasePrice.PriceDecimal)
            {
                updateCanRack();
            }
        }

        private void button_selectSoda_click(object sender, EventArgs e)
        {
            Button b = (Button)sender;
            if (b != null)
            {
                switch (b.Name)
                {

                    case "button_select_regular":
                        vendSodaOf(Flavor.REGULAR);
                        break;
                    case "button_select_orange":
                        vendSodaOf(Flavor.ORANGE);
                        break;
                    case "button_select_lemon":
                        vendSodaOf(Flavor.LEMON);
                        break;
                    default:
                        break;
                }
            }
        }

        private void vendSodaOf(Flavor f)
        {
            canRack.RemoveACanOf(f);
            decimal change = coinBox.ValueOf - purchasePrice.PriceDecimal;
            string msg = string.Format("{0} flavored soda has been vended to you.", f);
            if (change > 0)
            {
                msg += string.Format("\n{0} has been returned.", change);
            }
            MessageBox.Show(msg);
            emptyCoinBox(coinBox);
            updateCanRack();
            updateTotalInserted();
        }

        private void tabControl_click(object sender, EventArgs e)
        {
            displayListViewCanRack();
            displayListViewCoinBox();
        }

        private void displayListViewCanRack()
        {
            listViewCanRack.Items.Clear();
            foreach (Flavor f in FlavorOps.AllFlavors)
            {
                ListViewItem item = new ListViewItem(f.ToString());
                item.SubItems.Add(canRack[f].ToString());
                listViewCanRack.Items.Add(item);
            }
        }

        private void displayListViewCoinBox()
        {
            listViewCoinBox.Items.Clear();
            foreach (Coin.Denomination c in Coin.AllDenominations)
            {
                ListViewItem item = new ListViewItem(c.ToString());
                item.SubItems.Add(coinBox.coinCount(c).ToString());
                item.SubItems.Add(string.Format("{0:c}", Coin.ValueOfCoin(c)));
                decimal totalValue = coinBox.coinCount(c) * Coin.ValueOfCoin(c);
                item.SubItems.Add(string.Format("{0:c}", totalValue));
                listViewCoinBox.Items.Add(item);
            }
            label_totalInsertedService.Text = string.Format("{0:c}", coinBox.ValueOf);

        }

        private void button_emptyCoinBox_click(object sender, EventArgs e)
        {
            coinBox.Withdraw(coinBox.ValueOf);
            displayListViewCoinBox();
            updateTotalInserted();
            updateCanRack();
        }

        private void button_refillCanRack_click(object sender, EventArgs e)
        {
            canRack.FillTheCanRack();
            displayListViewCanRack();
        }
    }
}
